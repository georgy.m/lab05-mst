import unittest
import mst

_graph_empty = ''
_graph_simple = 'a b 1'
_graph = '''
A B 2
B C 5
B D 2
C D 4
D E 3
C E 3
D F 7
E F 8
'''


class TestEdgesReadings(unittest.TestCase):
    def read(self, desc: str, corr: set):
        edges = mst.read_edges(desc)
        for a, b, w in edges:
            assert (a, b, w) in corr or (b, a, w) in corr

    def test_empty(self):
        corr = {}
        self.read(_graph_empty, corr)

    def test_simple(self):
        corr = {('a', 'b', 1.0)}
        self.read(_graph_simple, corr)

    def test_graph(self):
        corr = {('A', 'B', 2.0), ('B', 'C', 5.0), ('B', 'D', 2.0),
                ('C', 'D', 4.0), ('D', 'E', 3.0), ('C', 'E', 3.0),
                ('D', 'F', 7.0), ('E', 'F', 8.0)}
        self.read(_graph, corr)


class TestMST(unittest.TestCase):
    def findmst(self, desc: str, corr_mst: set):
        g = mst.read_edges(desc)
        m = mst.get_mst(g)
        assert len(m) == len(corr_mst)
        for t in m:
            assert t in corr_mst or t[::-1] in corr_mst

    def test_empty(self):
        corr = {}
        self.findmst(_graph_empty, corr)

    def test_simple(self):
        corr = {('a', 'b')}
        self.findmst(_graph_simple, corr)

    def test_graph(self):
        corr = {('A', 'B'), ('B', 'D'), ('D', 'E'), ('C', 'E'), ('D', 'F')}
        self.findmst(_graph, corr)


if __name__ == '__main__':
    unittest.main()
