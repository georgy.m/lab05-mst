'''This module is responsible for parsing graphs and solving spanning tree
problem via Kruskal algorith.'''


def read_edges(g: str):
    '''Parses a graph from a textual description into a list of edges.
    A graph is submitted to the functions using the following syntax:
        nodeA nodeB W
        etc
    which means that nodeA is connected to nodeB with weight W.
    Keyword arguments:
        s     -- A string description of a graph.
        undir -- If True, then graph is assummed to be undirected.
    Returns:
        A list of tuples.
    '''
    res = list()
    for line in g.splitlines():
        line = line.strip()
        if line:
            a, b, w = line.split(' ')
            w = float(w)
            res.append((a, b, w))
    return res


def get_mst(g: list):
    '''Retrieves a minimal spanning tree from graph.
    Keyword arguments:
        g -- A list representation of graph as returned by parse_graph.
    Returns:
        List of edges that form MST.
    '''
    ds = DisjointSets()
    g = sorted(g, key=lambda x: x[-1])
    mst = list()
    for a, b, _ in g:
        i, j = ds.find(a), ds.find(b)
        if i == -1:
            ds.make_set(a)
            i = a
        if j == -1:
            ds.make_set(b)
            j = b
        if i != j:
            ds.union(i, j)
            mst.append((a, b))
    return mst


class DisjointSets():
    '''Implementation of DS structure'''
    def __init__(self):
        self._parent = dict()
        self._rank = dict()

    def __contains__(self, x):
        return x in self._parent

    def make_set(self, x):
        '''Adds element to a data structure. It is assumed that x is not
        present in any of sets.
        Keyword arguments:
            x -- Element to add.
        '''
        self._parent[x] = x
        self._rank[x] = 0

    def union(self, a, b):
        '''Joins two sets together.
        Keyword arguments:
            a -- parent of first set.
            b -- parent of second set.
        '''
        a, b = self.find(a), self.find(b)
        if a != b:
            rank_a, rank_b = self._rank[a], self._rank[b]
            if rank_a > rank_b:
                a, b = b, a
            elif rank_a == rank_b:
                self._rank[a] += 1
            self._parent[b] = a

    def find(self, x):
        '''Find an ID of set that contains element x.
        Keyword arguments:
            x -- element.
        Returns:
            Parent of set that contains x. -1 if no set contains x.
        '''
        v = self._parent.get(x, -1)
        if v in (x, -1):
            return v
        return self.find(v)
